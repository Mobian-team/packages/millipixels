#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define NUM_WB 8

extern const char* WB_ILLUMINANTS[NUM_WB];
extern const float WB_WHITEPOINTS[NUM_WB][3];

const char* wb_illuminants_get(unsigned i);

float wb_whitepoints_get(unsigned i, unsigned y);

#ifdef __cplusplus
}
#endif
