#include <cstdio>
#include <string>
#include <sstream>
#include "src/ui.h"
#include "wb.h"


extern "C" {
void draw_controls_safe(cairo_t *cr, bool has_focus) {
	std::ostringstream iso;
	std::ostringstream shutterangle;
	std::ostringstream f;
	std::ostringstream wb_text;

	if (exposure_is_manual) {
		shutterangle << exposure;
	} else if (sw_auto) {
		shutterangle << "a " << exposure;
	} else {
		shutterangle << "auto";
	}

	if (gain_is_manual) {
		iso << gain;
	} else if (sw_auto) {
		iso << "a " <<  gain;
	} else {
		iso << "auto";
	}

	if (focus_is_manual) {
		f << focus;
	} else {
		f << "a " << focus;
	}

	if (wb_is_manual) {
		wb_text << wb_illuminants_get(wb);
	} else {
		wb_text << "a " << wb_illuminants_get(wb);
	}

	cairo_set_source_rgba(cr, 0, 0, 0, 0.0);
	cairo_paint(cr);

	// Draw the outlines for the headings
	cairo_select_font_face(cr, "sans-serif", CAIRO_FONT_SLANT_NORMAL,
				   CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 9);
	cairo_set_source_rgba(cr, 0, 0, 0, 1);

	cairo_move_to(cr, gain_x, desc_y);
	cairo_text_path(cr, "Gain");
	cairo_stroke(cr);

	cairo_move_to(cr, exposure_x, desc_y);
	cairo_text_path(cr, "Exposure");
	cairo_stroke(cr);

	cairo_move_to(cr, balance_x, desc_y);
	cairo_text_path(cr, "Balance");
	cairo_stroke(cr);

	if (has_focus) {
		cairo_move_to(cr, focus_x, desc_y);
		cairo_text_path(cr, "Focus");
		cairo_stroke(cr);
	}

	// Draw the fill for the headings
	cairo_set_source_rgba(cr, 1, 1, 1, 1);
	cairo_move_to(cr, gain_x, desc_y);
	cairo_show_text(cr, "Gain");
	cairo_move_to(cr, exposure_x, desc_y);
	cairo_show_text(cr, "Exposure");
	cairo_move_to(cr, balance_x, desc_y);
	cairo_show_text(cr, "Balance");
	if (has_focus) {
		cairo_move_to(cr, focus_x, desc_y);
		cairo_show_text(cr, "Focus");
	}

	// Draw the outlines for the values
	cairo_select_font_face(cr, "sans-serif", CAIRO_FONT_SLANT_NORMAL,
				   CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size(cr, 11);
	cairo_set_source_rgba(cr, 0, 0, 0, 1);

	cairo_move_to(cr, gain_x, value_y);
	cairo_text_path(cr, iso.str().c_str());
	cairo_stroke(cr);

	cairo_move_to(cr, exposure_x, value_y);
	cairo_text_path(cr, shutterangle.str().c_str());
	cairo_stroke(cr);

	cairo_move_to(cr, balance_x, value_y);
	cairo_text_path(cr, wb_text.str().c_str());
	cairo_stroke(cr);

	if (has_focus) {
		cairo_move_to(cr, focus_x, value_y);
		cairo_text_path(cr, f.str().c_str());
		cairo_stroke(cr);
	}

	// Draw the fill for the values
	cairo_set_source_rgba(cr, 1, 1, 1, 1);
	cairo_move_to(cr, gain_x, value_y);
	cairo_show_text(cr, iso.str().c_str());
	cairo_move_to(cr, exposure_x, value_y);
	cairo_show_text(cr, shutterangle.str().c_str());
	cairo_move_to(cr, balance_x, value_y);
	cairo_show_text(cr, wb_text.str().c_str());
	if (has_focus) {
		cairo_move_to(cr, focus_x, value_y);
		cairo_show_text(cr, f.str().c_str());
	}

	cairo_move_to(cr, debug_x, value_y);
	cairo_text_path(cr, debug_message);
	cairo_stroke(cr);
}
}
