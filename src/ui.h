#include <cairo/cairo.h>

#ifdef __cplusplus
extern "C" {
#endif

const int gain_x = 16;
const int exposure_x = gain_x + 60;
const int balance_x = exposure_x + 60;
const int focus_x = balance_x + 60;
const int debug_x = focus_x + 60;

const int desc_y = 16;
const int value_y = 26;
const int max_y = 32;

const bool sw_auto = true;

// Those are actually stored in main.
// TODO: They should actually end up in some sort of a struct.
extern char *debug_message;
extern int focus;
extern bool focus_is_manual;
extern int wb;
extern bool wb_is_manual;
extern bool exposure_is_manual;
extern int exposure;
extern bool gain_is_manual;
extern int gain;


void draw_controls_safe(cairo_t *cr, bool has_focus);
#ifdef __cplusplus
}
#endif
