/*! Hack to prevent the focus mechanism from resetting.
This is done in Rust rather than C because of easier string/process handling.*/

pub mod c {
    use super::*;

    use std::ffi:: OsStr;
    use std::fs::File;
    use std::io;
    use std::os::raw::c_int;
    use std::os::unix::ffi::OsStrExt;
    use std::os::unix::io::IntoRawFd;
    use std::process::Command;

    /// Returns -1 on error.
    #[no_mangle]
    pub extern "C" fn open_focus_fd() -> c_int {
        try_open_focus_fd()
            .unwrap_or_else(|(msg, e)| {
                eprintln!("{}: {:?}", msg, e);
                -1
            })
    }

    fn try_open_focus_fd() -> Result<c_int, (&'static str, io::Error)> {
        let output = Command::new("media-ctl")
            .args(&["-d", "platform:30b80000.csi", "-e", "dw9714 3-000c"])
            .output()
            .map_err(|e| ("Failed to find the focus subdevice", e))?;
        let stdout = output.stdout;
        let stripped_stdout = strip_newlines(&stdout);
        let subdevice_path = OsStr::from_bytes(stripped_stdout);
        let subdevice = File::open(subdevice_path)
            .map_err(|e| ("No such path", e))?;
        let fd = subdevice.into_raw_fd();
        Ok(fd)
    }
    // closing the focus fd can be done in C using a simple close(fd) call.
    
    #[no_mangle]
    pub extern "C" fn set_focus(absolute: i32) {
        // We don't really care about the result above printing the message.
        let _ = Command::new("v4l2-ctl")
            .args(&[
                "--media-bus-info", "platform:30b80000.csi",
                "--device", "dw9714 3-000c",
                "--set-ctrl", &format!("focus_absolute={}", absolute),
            ])
            .spawn()
            .map_err(|e| eprintln!("Failed to set focus: {:?}", e));
    }
}

const NEWLINE: u8 = 10;

fn strip_newlines(v: &[u8]) -> &[u8] {
    let first_newline = v.iter()
        .rposition(|c| *c != NEWLINE)
        .map(|i| i + 1)
        .unwrap_or(0);
    &v[..first_newline]
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn a() {
        assert_eq!(strip_newlines(&[]), &[]);
        assert_eq!(strip_newlines(&[10]), &[]);
        assert_eq!(strip_newlines(&[5, 10]), &[5]);
        assert_eq!(strip_newlines(&[5, 10, 10]), &[5]);
        assert_eq!(strip_newlines(&[10, 10, 10]), &[]);
    }
}