#pragma once

#include "camera_config.h"

#include "io_pipeline.h"

struct mp_process_pipeline_state {
	const struct mp_camera_config *camera;
	MPCameraMode mode;
	bool is_present;

	int burst_length;

	int preview_width;
	int preview_height;

	int gain_max;
	int gain_min;

	int exposure_max;
	int exposure_min;

	bool has_auto_focus_continuous;
	bool has_auto_focus_start;

	int wb;

	struct control_state controls;
};

struct image_stats {
	int exposure_adjust, balance_adjust;
	uint64_t sharpness;
};

void mp_process_pipeline_start();
void mp_process_pipeline_stop();

void mp_process_pipeline_process_image(MPImage image);
void mp_process_pipeline_capture();
void mp_process_pipeline_update_state(const struct mp_process_pipeline_state *state);

void on_movie_start(void);
void on_movie_stop(void);
