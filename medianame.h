#include <sys/time.h>
#include <stdint.h>

static inline uintmax_t time_usec(void)
{
	struct timeval t;
	gettimeofday(&t, NULL);

	return t.tv_sec * 1000000 + t.tv_usec;
}

static inline void get_name(char *buf, int len, char *dir, char *templ)
{
	snprintf(buf, len, "%s/%jd.%s", dir, time_usec(), templ);
}
