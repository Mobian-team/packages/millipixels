/* Heavily based on postprocessd: https://git.sr.ht/~martijnbraam/postprocessd */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <dirent.h>
#include <libraw/libraw.h>
#include <tiffio.h>
#include <jpeglib.h>
#include <libexif/exif-data.h>
#include <time.h>
#include <unistd.h>
#include <stdbool.h>

#define DCPTAG_PROFILE_HUE_SAT_MAP_DIMS 50937
#define DCPTAG_PROFILE_HUE_SAT_MAP_DATA_1 50938
#define DCPTAG_PROFILE_HUE_SAT_MAP_DATA_2 50939
#define DCPTAG_PROFILE_TONE_CURVE 50940
#define DCPTAG_FORWARD_MATRIX_1 50964
#define DCPTAG_FORWARD_MATRIX_2 50965

#define JPEG_APP1 JPEG_APP0+1

struct Imagedata {
		uint32_t width;
		uint32_t height;
		uint8_t bitspersample;
		char *make;
		char *model;
		char *software;
		int orientation;
		char *datetime;
		uint16_t exposure_program;
		float exposure_time;
		uint16_t isospeed;
		int flash;
		float fnumber;
		float focal_length;
		uint16_t focal_length_35mm;
};

void
err(char *message)
{
    fprintf(stderr, "%s (%s)\n", message, strerror(errno));
}

static void
register_custom_tiff_tags(TIFF *tif)
{
	static const TIFFFieldInfo custom_fields[] = {
		{DCPTAG_FORWARD_MATRIX_1,           -1, -1, TIFF_SRATIONAL, FIELD_CUSTOM, 1, 1,
			"ForwardMatrix1"},
		{DCPTAG_FORWARD_MATRIX_2,           -1, -1, TIFF_SRATIONAL, FIELD_CUSTOM, 1, 1,
			"ForwardMatrix2"},
		{DCPTAG_PROFILE_TONE_CURVE,         -1, -1, TIFF_SRATIONAL, FIELD_CUSTOM, 1, 1,
			"ProfileToneCurve"},
		{DCPTAG_PROFILE_HUE_SAT_MAP_DIMS,   -1, -1, TIFF_SRATIONAL, FIELD_CUSTOM, 1, 1,
			"ProfileHueSatMapDims"},
		{DCPTAG_PROFILE_HUE_SAT_MAP_DATA_1, -1, -1, TIFF_SRATIONAL, FIELD_CUSTOM, 1, 1,
			"ProfileHueSatMapData1"},
		{DCPTAG_PROFILE_HUE_SAT_MAP_DATA_2, -1, -1, TIFF_SRATIONAL, FIELD_CUSTOM, 1, 1,
			"ProfileHueSatMapData2"},
	};

    // Add missing dng fields
    TIFFMergeFieldInfo(tif, custom_fields,
        sizeof(custom_fields) / sizeof(custom_fields[0]));
}

libraw_processed_image_t *
debayer_file(char *filename, bool halfsize, bool denoise, int interpolation)
{
    libraw_data_t *raw;
    int ret;

    raw = libraw_init(0);
    if (libraw_open_file(raw, filename) != LIBRAW_SUCCESS) {
        err("could not open file");
        libraw_close(raw);
    }

    if (libraw_unpack(raw) != LIBRAW_SUCCESS) {
        err("could not unpack file");
        libraw_close(raw);
    }

    raw->params.use_camera_wb = 1;
    raw->params.highlight = 2;
    raw->params.fbdd_noiserd = denoise;
    if (halfsize) {
       raw->params.half_size = 1;
    } else {
       raw->params.user_qual = interpolation;
    }

    if (libraw_dcraw_process(raw) != LIBRAW_SUCCESS) {
        err("could not process file");
        libraw_free_image(raw);
        libraw_close(raw);
    }

    libraw_processed_image_t *proc_img = libraw_dcraw_make_mem_image(raw, &ret);
    libraw_free_image(raw);

    if (!proc_img) {
        err("could not export image");
        libraw_close(raw);
    }
    libraw_recycle(raw);
    libraw_close(raw);
    return proc_img;
}

void
save_jpeg(char *path, libraw_processed_image_t *data, int quality, ExifData *exif)
{
    unsigned char *exif_data;
    unsigned int exif_len;
    FILE *out = fopen(path, "wb");
    if (!out) {
        err("could not open target file");
        exit(1);
    }

    struct jpeg_compress_struct jpeg;
    struct jpeg_error_mgr error_mgr;
    jpeg.err = jpeg_std_error(&error_mgr);
    jpeg_create_compress(&jpeg);
    jpeg_stdio_dest(&jpeg, out);
    jpeg.image_width = data->width;
    jpeg.image_height = data->height;
    jpeg.input_components = 3;
    jpeg.in_color_space = JCS_RGB;
    jpeg_set_defaults(&jpeg);
    jpeg_set_quality(&jpeg, quality, 1);
    jpeg_start_compress(&jpeg, 1);

    // Write exif
    exif_data_save_data(exif, &exif_data, &exif_len);
    jpeg_write_marker(&jpeg, JPEG_APP1, exif_data, exif_len);

    // Write image data
    JSAMPROW row_pointer;
    int row_stride = jpeg.image_width * jpeg.input_components;

    while (jpeg.next_scanline < jpeg.image_height) {
        row_pointer = (JSAMPROW) &data->data[jpeg.next_scanline * row_stride];
        jpeg_write_scanlines(&jpeg, &row_pointer, 1);
    }

    jpeg_finish_compress(&jpeg);
    jpeg_destroy_compress(&jpeg);
    fclose(out);
}

static ExifEntry *
init_tag(ExifData *exif, ExifIfd ifd, ExifTag tag)
{
    ExifEntry *entry;
    /* Return an existing tag if one exists */
    if (!((entry = exif_content_get_entry(exif->ifd[ifd], tag)))) {
        /* Allocate a new entry */
        entry = exif_entry_new();
        assert(entry != NULL); /* catch an out of memory condition */
        entry->tag = tag; /* tag must be set before calling
				 exif_content_add_entry */

        /* Attach the ExifEntry to an IFD */
        exif_content_add_entry(exif->ifd[ifd], entry);

        /* Allocate memory for the entry and fill with default data */
        exif_entry_initialize(entry, tag);

        /* Ownership of the ExifEntry has now been passed to the IFD.
         * One must be very careful in accessing a structure after
         * unref'ing it; in this case, we know "entry" won't be freed
         * because the reference count was bumped when it was added to
         * the IFD.
         */
        exif_entry_unref(entry);
    }
    return entry;
}

void
exif_set_string(ExifEntry *ed, const char *s, size_t size)
{
    if (ed->data) {
        free(ed->data);
    }
    ed->components = size + 1;
    ed->size = sizeof(char) * ed->components;
    ed->data = (unsigned char *) malloc(ed->size);
    if (!ed->data) {
        err("Could not allocate exif string");
        exit(1);
    }
    strncpy((char *) ed->data, (char *) s, size);
    exif_entry_fix(ed);
}

ExifData *
create_exif(struct Imagedata data)
{
    ExifEntry *entry;
    ExifRational rational;
    long denominator = 100000;
    ExifData *exif = exif_data_new();
    if (!exif) {
        err("Could not initialize libexif");
    }
    exif_data_set_option(exif, EXIF_DATA_OPTION_FOLLOW_SPECIFICATION);
    exif_data_set_data_type(exif, EXIF_DATA_TYPE_COMPRESSED);
    exif_data_set_byte_order(exif, EXIF_BYTE_ORDER_INTEL);
    exif_data_fix(exif);

    // Width
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_PIXEL_X_DIMENSION);
    exif_set_long(entry->data, EXIF_BYTE_ORDER_INTEL, data.width);

    // Height
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_PIXEL_Y_DIMENSION);
    exif_set_long(entry->data, EXIF_BYTE_ORDER_INTEL, data.height);

    // Colorspace, 1=sRGB
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_COLOR_SPACE);
    exif_set_long(entry->data, EXIF_BYTE_ORDER_INTEL, 1);

    // Exposure program, enum
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_EXPOSURE_PROGRAM);
    exif_set_long(entry->data, EXIF_BYTE_ORDER_INTEL, data.exposure_program);

    // Camera make
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_MAKE);
    exif_set_string(entry, data.make, strlen(data.make));

    // Camera model
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_MODEL);
    exif_set_string(entry, data.model, strlen(data.model));

    // Processing software
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_SOFTWARE);
    exif_set_string(entry, data.software, strlen(data.software));

    // Various datetime fields
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_DATE_TIME);
    exif_set_string(entry, data.datetime, strlen(data.datetime));
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_DATE_TIME_DIGITIZED);
    exif_set_string(entry, data.datetime, strlen(data.datetime));
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_DATE_TIME_ORIGINAL);
    exif_set_string(entry, data.datetime, strlen(data.datetime));

    // Exposure time
    rational.numerator = (long) ((double) data.exposure_time * denominator);
    rational.denominator = denominator;
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_EXPOSURE_TIME);
    exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL, rational);

    // fnumber
    rational.numerator = (long) ((double) data.fnumber * denominator);
    rational.denominator = denominator;
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_FNUMBER);
    exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL, rational);

    // focal length
    rational.numerator = (long) ((double) data.focal_length * denominator);
    rational.denominator = denominator;
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_FOCAL_LENGTH);
    exif_set_rational(entry->data, EXIF_BYTE_ORDER_INTEL, rational);

    // focal length, 35mm equiv
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM);
    exif_set_short(entry->data, EXIF_BYTE_ORDER_INTEL, data.focal_length_35mm);

    // ISO
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_ISO_SPEED_RATINGS);
    exif_set_long(entry->data, EXIF_BYTE_ORDER_INTEL, data.isospeed);

    // Flash
    entry = init_tag(exif, EXIF_IFD_EXIF, EXIF_TAG_FLASH);
    exif_set_short(entry->data, EXIF_BYTE_ORDER_INTEL, data.flash);
    return exif;
}

struct Imagedata
read_exif(char *filename)
{
    struct Imagedata imagedata;
    uint16_t subifd_count;
    uint32_t *subifd_offsets;
    uint32_t exif_offset;
    uint32_t value_count;
    uint16_t *short_array;
    char *temp;

    TIFF *im = TIFFOpen(filename, "r");

    if (im == NULL) {
        fprintf(stderr, "Could not open tiff");
        exit(1);
    }

    if (TIFFGetField(im, TIFFTAG_MAKE, &temp) != 1) {
        err("failed to read TIFFTAG_MAKE");
    }
    imagedata.make = strdup(temp);

    if (TIFFGetField(im, TIFFTAG_MODEL, &temp) != 1) {
        err("failed to read TIFFTAG_MODEL");
    }
    imagedata.model = strdup(temp);

    if (TIFFGetField(im, TIFFTAG_SOFTWARE, &temp) != 1) {
        err("failed to read TIFFTAG_SOFTWARE");
    }
    imagedata.software = strdup(temp);

    if (TIFFGetField(im, TIFFTAG_DATETIME, &temp) != 1) {
        err("failed to read TIFFTAG_DATETIME");
    }
    imagedata.datetime = strdup(temp);

    if (TIFFGetField(im, TIFFTAG_ORIENTATION, &imagedata.orientation) != 1) {
        err("failed to read TIFFTAG_ORIENTATION");
    }

    // Get the EXIF directory for the rest of the metadata
    if (TIFFGetField(im, TIFFTAG_EXIFIFD, &exif_offset) != 1) {
        err("failed to read TIFFTAG_EXIFIFD");
    }

    // Read the actual picture instead of the embedded thumbnail
    if (TIFFGetField(im, TIFFTAG_SUBIFD, &subifd_count, &subifd_offsets)) {
        if (subifd_count > 0) {
            TIFFSetSubDirectory(im, subifd_offsets[0]);
        }
    }

    if (TIFFGetField(im, TIFFTAG_IMAGELENGTH, &imagedata.height) != 1) {
        err("failed to read TIFFTAG_IMAGELENGTH");
    }
    if (TIFFGetField(im, TIFFTAG_IMAGEWIDTH, &imagedata.width) != 1) {
        err("failed to read TIFFTAG_IMAGEWIDTH");
    }
    if (TIFFGetField(im, TIFFTAG_BITSPERSAMPLE, &imagedata.bitspersample) != 1) {
        err("failed to read TIFFTAG_BITSPERSAMPLE");
    }

    // Read the exif directory
    TIFFReadEXIFDirectory(im, exif_offset);

    if (TIFFGetField(im, EXIFTAG_EXPOSUREPROGRAM, &imagedata.exposure_program) != 1) {
        err("failed to read EXIFTAG_EXPOSUREPROGRAM");
    }

    if (TIFFGetField(im, EXIFTAG_EXPOSURETIME, &imagedata.exposure_time) != 1) {
        err("failed to read EXIFTAG_EXPOSURETIME");
    }

    if (TIFFGetField(im, EXIFTAG_PHOTOGRAPHICSENSITIVITY, &value_count, &short_array) != 1) {
        err("failed to read EXIFTAG_PHOTOGRAPHICSENSITIVITY");
    }else {
        imagedata.isospeed = short_array[0];
    }

    if (TIFFGetField(im, EXIFTAG_FNUMBER, &imagedata.fnumber) != 1) {
        err("failed to read EXIFTAG_FNUMBER");
    }

    if (TIFFGetField(im, EXIFTAG_FOCALLENGTH, &imagedata.focal_length) != 1) {
        err("failed to read EXIFTAG_FOCALLENGTH");
    }

    if (TIFFGetField(im, EXIFTAG_FOCALLENGTHIN35MMFILM, &imagedata.focal_length_35mm) != 1) {
        err("failed to read EXIFTAG_FOCALLENGTHIN35MMFILM");
    }

    if (TIFFGetField(im, EXIFTAG_FLASH, &imagedata.flash) != 1) {
        err("failed to read EXIFTAG_FLASH");
    }
    TIFFClose(im);
    return imagedata;
}

void
postprocess_setup()
{
    TIFFSetTagExtender(register_custom_tiff_tags);
}


void
postprocess_single(char *in_path, char *out_path, int quality, int verbose, bool halfsize, bool denoise, int interpolation)
{
    libraw_processed_image_t *decoded;
    struct Imagedata imagedata;
    ExifData *exif;
    clock_t timer;

    // Parse exif data from original file
    timer = clock();
    imagedata = read_exif(in_path);
    exif = create_exif(imagedata);
    if (verbose) {
        printf("[%.1fms] %s\n", (float) (clock() - timer) / CLOCKS_PER_SEC * 1000, "exif read");
    }

    // Convert the sensor data to rgb
    timer = clock();
    decoded = debayer_file(in_path, halfsize, denoise, interpolation);
    if (verbose) {
        printf("[%.1fms] %s\n", (float) (clock() - timer) / CLOCKS_PER_SEC * 1000, "debayer");
    }

    // Export the final jpeg with the original exif data
    timer = clock();
    save_jpeg(out_path, decoded, quality, exif);
    if (verbose) {
        printf("[%.1fms] %s\n", (float) (clock() - timer) / CLOCKS_PER_SEC * 1000, "export");
    }
}



void
print_usage(char *name)
{
    fprintf(stderr, "usage: %s [-v] [-q quality] [-i method] [-n] [-s] INPUT OUTPUT\n", name);
    fprintf(stderr, "Post-process a single .dng bayer raw file to a jpeg file.\n\n");
    fprintf(stderr, "Mandatory arguments\n");
    fprintf(stderr, "INPUT    Input filename, should be a .dng file\n");
    fprintf(stderr, "OUTPUT   Output filename, should be a .jpg file\n\n");

    fprintf(stderr, "Optional arguments\n");
    fprintf(stderr, "  -v     Show verbose debugging output\n");
    fprintf(stderr, "  -q 90  Set the output jpeg quality [0-100]\n");
    fprintf(stderr, "  -i -1  Set the interpolation method (see libraw's user_qual)\n");
    fprintf(stderr, "  -n     Enable FBDD noise reduction before demosaic\n");
    fprintf(stderr, "  -s     Output the image in 50%% size\n\n");
}

int
main(int argc, char *argv[])
{
    int opt;
    long temp;
    bool verbose = false;
    int quality = 90;
    int interpolation = -1;
    bool halfsize = false;
    bool denoise = false;
    char *endptr;
    char *input_path;
    char *output_path;

    while ((opt = getopt(argc, argv, "vnsq:i:h")) != -1) {
        switch (opt) {
            case 'v':
                verbose = 1;
                break;
            case 'n':
                denoise = 1;
                break;
            case 's':
                halfsize = 1;
                break;
            case 'q':
                temp = strtol(optarg, &endptr, 0);
                if (errno == ERANGE || *endptr != '\0' || optarg == endptr) {
                    fprintf(stderr, "Invalid value for -q\n");
                    print_usage(argv[0]);
                    return 1;
                }
                if (temp < 0 || temp > 100) {
                    fprintf(stderr, "Quality out of range, valid values are 0-100\n");
                    print_usage(argv[0]);
                    return 1;
                }
                quality = (int) temp;
                break;
            case 'i':
                temp = strtol(optarg, &endptr, 0);
                if (errno == ERANGE || *endptr != '\0' || optarg == endptr) {
                    fprintf(stderr, "Invalid value for -i\n");
                    print_usage(argv[0]);
                    return 1;
                }
                interpolation = (int) temp;
                break;
            case 'h':
                print_usage(argv[0]);
                return 0;
            case '?':
                print_usage(argv[0]);
                return 1;
        }
    }

    if (argc < optind + 2) {
        fprintf(stderr, "Missing image parameters\n");
        print_usage(argv[0]);
        return 1;
    }

    input_path = argv[optind];
    output_path = argv[optind + 1];

    postprocess_setup();
    postprocess_single(input_path, output_path, quality, verbose, halfsize, denoise, interpolation);
    return 0;
}
